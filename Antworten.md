# Textantworten

## b) 
Die Header-Dateien des Betriebssystems bleiben unber�cksichtig.
Der User hat keinen Zugriff auf die Standardlibrary und diese Header-Dateien, 
daher werden diese Dateien (normalerweise) nicht ver�ndert und eine 
Ber�cksichtigung als dependency im makefile wird unn�tig.

## c) 
Die Header-Datei zu `input.c` muss `main.c`, sowie die
dependency zu diesem bei `main.o` im makefile hinzugef�gt werden, damit
die Funktion erkannt werden kann.
Das Linken zur eigentlichen Funktion geschieht beim Erstellen der
editor.exe-Datei und muss deshalb nicht vorher ber�cksichtigt werden.

## e)
`clean` und `install` sind sogenannte `phony targets`. Das hei�t, dass
sie nur dazu genutzt werden Befehle auszuf�hren, selbst aber nie eine
ausf�hrbare Datei erstellen bzw. als ausf�hrbare Datei zur Verf�gung stehen.
Da sie auch (f�r gew�hnlich) keine dependencies besitzen, werden sie
bei jedem Aufruf (`make clean`, `make install`) auch ausgef�hrt.
G�be es allerdings Dateien mit diesem Namen im Verzeichnis, w�ren 
`clean` und `install`, da sie keine dependencies besitzen, immer aktuell
aus Sicht des Makefiles und die beiden Optionen w�rden nicht mehr
ausgef�hrt werden.
Besser ist es daher im Makefile `clean` und `install` als "phony" zu deklarieren,
um dieses Problem zu vermeiden (siehe makefile meiner L�sung).