CC = gcc		#GNU C-Compiler
CFLAGS = -Wall -g	#Compilerflags

-include depend.d

editor.exe : main.o input.o
	$(CC) $(CFLAGS) main.o input.o -o editor.exe

main.o : main.c utils.h types.h defs.h input.h
	$(CC) $(CFLAGS) -o main.o -c main.c

input.o : input.c types.h
	$(CC) $(CFLAGS) -o input.o -c input.c

.Phony: clean install

clean :
	rm -f *.o editor.exe

install : editor.exe
	cp editor.exe /usr/bin/
	chmod 555 /usr/bin/editor.exe

depend: main.c input.c utils.h types.h defs.h
	$(CC) -MM main.c input.c > depend.d